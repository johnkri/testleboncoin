//
//  Router.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

protocol WireframeDelegate {
    func createMainModule() -> MainViewController
}

final class Wireframe: WireframeDelegate {
    
    static let shared = Wireframe()
    
    private init() {}
    
    func createMainModule() -> MainViewController {
        
        let view = MainViewController()
        let apiClient = ApiClientImplementation(urlSessionConfiguration: URLSessionConfiguration.default, completionHandlerQueue: OperationQueue.main)
        let networkService = NetworkService(apiClient: apiClient)
        let presenter: PresenterDelegate & InteractorOutputDelegate = PresenterImplementation()
        let interactor: InteractorInputDelegate = InteractorImplementation()
        let wireframe: WireframeDelegate = Wireframe()
        
        view.presenter = presenter
        presenter.view = view
        presenter.wireframe = wireframe
        presenter.interactor = interactor
        interactor.presenter = presenter
        interactor.networkService = networkService
        
        return view
    }
        
}
