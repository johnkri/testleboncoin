//
//  Image.swift
//  TestLeBonCoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

public struct Image: Decodable {
    
    let small: String?
    let thumb: String?
}

