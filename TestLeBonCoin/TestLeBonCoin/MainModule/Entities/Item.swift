//
//  Item.swift
//  TestLeBonCoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

public struct Item: Decodable, Equatable {
    
    public static func == (lhs: Item, rhs: Item) -> Bool {
        return lhs.title == rhs.title
    }
    
    let imagesUrl: Image
    var id: Int
    var title: String
    var price: Int
    var creationDate: String
    var isUrgent: Bool
    var categoryId: Int
    var description: String
}

extension Item {
        enum CodingKeys: String, CodingKey {
            case id
            case title
            case price
            case creationDate = "creation_date"
            case isUrgent = "is_urgent"
            case categoryId = "category_id"
            case imagesUrl = "images_url"
            case description
        }
}

