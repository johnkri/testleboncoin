//
//  ItemWithCategoryName.swift
//  testleboncoin
//
//  Created by John Kricorian on 30/04/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

struct ItemWithCategoryName : Equatable {
    static func == (lhs: ItemWithCategoryName, rhs: ItemWithCategoryName) -> Bool {
        return lhs.title == rhs.title
    }
    
    let image: Image
    var id: Int
    var title: String
    var price: Int
    var creationDate: String
    var isUrgent: Bool
    let categoryName: String
    let description: String
}
