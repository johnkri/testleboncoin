//
//  Category.swift
//  TestLeBonCoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

public struct Category: Decodable {
    var id: Int
    var name: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        
    }
}

