//
//  Presenter.swift
//  testleboncoin
//
//  Created by John Kricorian on 24/04/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

protocol PresenterDelegate: class {
    func configure(cell: ItemTableViewCellDelegate, itemWithCategoryName: ItemWithCategoryName)
    func getResearchWith(_ searchText: String)
    func viewDidLoad()

    var view: MainViewDelegate? { get set }
    var interactor: InteractorInputDelegate? { get set }
    var wireframe: WireframeDelegate? { get set }
    var itemWithCategoryNames: [ItemWithCategoryName]? { get set }
}

protocol InteractorOutputDelegate: class {
    func didRetrieveItemsWithCategortName(_ itemWithCategoryNames: [ItemWithCategoryName])
    func onError()
}

class PresenterImplementation: PresenterDelegate {
    
    var itemWithCategoryNames: [ItemWithCategoryName]?
    
    var interactor: InteractorInputDelegate?
    var wireframe: WireframeDelegate?
    weak var view: MainViewDelegate?
    
    func viewDidLoad() {
        interactor?.retrieveItems()
        interactor?.retrieveCategories()
    }
        
    func configure(cell: ItemTableViewCellDelegate, itemWithCategoryName: ItemWithCategoryName) {
        cell.configure(itemWithCategoryName: itemWithCategoryName)
    }
    
    func getResearchWith(_ searchText: String) {
        interactor?.retrieveItemsWith(searchText: searchText)
    }
}

// MARK: - InteractorOutputDelegate
extension PresenterImplementation: InteractorOutputDelegate {
    
    func didRetrieveItemsWithCategortName(_ itemsWithCategoryName: [ItemWithCategoryName]) {
        
        if itemsWithCategoryName.isEmpty {
            view?.show(itemsWithCategoryName: [ItemWithCategoryName]())
        } else {
            self.itemWithCategoryNames = itemsWithCategoryName
            view?.show(itemsWithCategoryName: itemsWithCategoryName)
        }
    }
    
    func onError() {
        view?.show(itemsWithCategoryName: [ItemWithCategoryName]())
    }
    
}
