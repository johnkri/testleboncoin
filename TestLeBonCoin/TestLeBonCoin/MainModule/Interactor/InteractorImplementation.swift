//
//  InteractorProtocol.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

protocol InteractorInputDelegate: class {
    func retrieveItems()
    func retrieveCategories()
    func retrieveItemsWith(searchText: String)
    
    var presenter: InteractorOutputDelegate? { get set }
    var networkService: NetworkServiceDelegate? { get set }

}

class InteractorImplementation: InteractorInputDelegate {
        
    weak var presenter: InteractorOutputDelegate?
    var networkService: NetworkServiceDelegate?
    
    private var items = [Item]()
    private var categories = [Category]()
    private var itemWithCategoryNames = [ItemWithCategoryName]()
    private var searching = false
    private var filteredItemWithCategoryName = [ItemWithCategoryName]()
    
// MARK: - Public
     func retrieveItems() {
        networkService?.retrieveItems { [weak self] items in
            guard let self = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.8)  {
                switch items {
                case .success(let items):
                    guard let items = items else { return }
                    self.items = items
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
     func retrieveCategories() {
        networkService?.retrieveCategories { [weak self] categories in
            guard let self = self else { return }
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                switch categories {
                case .success(let categories):
                    guard let categories = categories else { return }
                    self.categories = categories
                    self.presenter?.didRetrieveItemsWithCategortName(self.createItemsWithCategoryNameWith(self.sorted(self.items), and: self.categories))
                case .failure(let error):
                    print(error)
                }
            }
        }
    }
    
     func retrieveItemsWith(searchText: String) {
        
        if searchText == "" {
            searching = false
            reloadDataWith(itemWithCategoryNames)
        } else {
            searching = true
            filteredItemWithCategoryName = filterItemWith(searchText, itemWithCategoryNames: itemWithCategoryNames)
            reloadDataWith(filteredItemWithCategoryName)
        }
    }
                 
     func reloadDataWith(_ items: [ItemWithCategoryName]) {
        if searching {
            presenter?.didRetrieveItemsWithCategortName(items)
        } else {
            presenter?.didRetrieveItemsWithCategortName(items)
        }
    }
    
    public func createItemsWithCategoryNameWith(_ items: [Item], and categories: [Category]) -> [ItemWithCategoryName] {
                                
        sorted(items).forEach { (item) in
            let categoryIndex = item.categoryId - 1
            guard categories.indices.contains(categoryIndex) else { return }
            let itemWithCategoryName = ItemWithCategoryName(image: item.imagesUrl, id: item.id, title: item.title, price: item.price, creationDate: item.creationDate, isUrgent: item.isUrgent, categoryName: categories[categoryIndex].name, description: item.description)
            itemWithCategoryNames.append(itemWithCategoryName)
        }

        return itemWithCategoryNames
    }
        
     public func sorted(_ items: [Item]) -> [Item] {
        
        return items.sorted {
            if $0.isUrgent != $1.isUrgent {
                return $0.isUrgent && !$1.isUrgent
            }
            return $0.creationDate < $1.creationDate
        }
    }
    
    public func filterItemWith(_ searchText: String, itemWithCategoryNames: [ItemWithCategoryName]) -> [ItemWithCategoryName] {
        filteredItemWithCategoryName = itemWithCategoryNames.filter { $0.categoryName.lowercased().prefix(searchText.count) == searchText.lowercased() }
        
        return filteredItemWithCategoryName
    }
}



