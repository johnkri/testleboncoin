//
//  ItemTableViewCell.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import UIKit

protocol ItemTableViewCellDelegate: class {
    func configure(itemWithCategoryName: ItemWithCategoryName)
}

class ItemTableViewCell: UITableViewCell {
        
    private var iconImageView : UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "Image"))
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    private var isUrgentImageView : UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "non_urgent"))
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
    
    private var titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.numberOfLines = 0
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textAlignment = .left
        return lbl
    }()
    
    private var priceLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textAlignment = .left
        return lbl
    }()
    
    
    private var dateLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textAlignment = .left
        return lbl
    }()
    
    private var categoryLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.systemFont(ofSize: 12)
        lbl.textAlignment = .left
        return lbl
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUp()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    
        
    // MARK: - Private

    private func setUp() {
        selectionStyle = .none
        let vStackView = UIStackView(verticalViews: [dateLabel, categoryLabel, titleLabel, priceLabel], spacing: 8.0)
        let hStackView = UIStackView(horizontalViews: [iconImageView, vStackView, isUrgentImageView], spacing: 8.0)
        contentView.addSubview(hStackView)
        hStackView.alignment = .center
        hStackView.pinToSuperview(insets: UIEdgeInsets(value: 16.0))

        NSLayoutConstraint.activate([
            iconImageView.widthAnchor.constraint(equalToConstant: 40.0),
            iconImageView.heightAnchor.constraint(equalToConstant: 40.0),
            isUrgentImageView.widthAnchor.constraint(equalToConstant: 24.0),
            isUrgentImageView.heightAnchor.constraint(equalToConstant: 24.0)
        ])
    }
}

extension ItemTableViewCell: ItemTableViewCellDelegate {
    
    func configure(itemWithCategoryName: ItemWithCategoryName) {
        titleLabel.text = itemWithCategoryName.title
        iconImageView.downloaded(from: itemWithCategoryName.image.small)
        priceLabel.text = "\(itemWithCategoryName.price) euros"
        dateLabel.text = itemWithCategoryName.creationDate.convertDateFormat()
        categoryLabel.text = itemWithCategoryName.categoryName
        if itemWithCategoryName.isUrgent {
          isUrgentImageView.image = #imageLiteral(resourceName: "urgent")
        } else {
            isUrgentImageView.image = #imageLiteral(resourceName: "non_urgent")
        }
    }
}
