//
//  ViewController.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import UIKit

protocol MainViewDelegate: class {
    func show(itemsWithCategoryName: [ItemWithCategoryName])
    func failure(error: Error)
}

final class MainViewController: UIViewController {
    
    var tableView = UITableView()
    var presenter: PresenterDelegate!
    let allTitle = "All Categories"
    var itemsWithCategoryName = [ItemWithCategoryName]()
    let child = SpinnerViewController()
    
    private func configureCategoryTextField() {
        let searchBar =  UISearchBar(frame: CGRect(x: view.frame.midX - 140, y: 120, width: 280, height: 40))
        searchBar.placeholder = "Enter category here"
        searchBar.autocorrectionType = UITextAutocorrectionType.no
        searchBar.keyboardType = UIKeyboardType.default
        searchBar.returnKeyType = UIReturnKeyType.done
        searchBar.delegate = self
        self.view.addSubview(searchBar)
    }
    
    private func setupTableView() {
        
        tableView = UITableView(frame: CGRect(x: 0, y: 180, width: view.frame.width, height: view.frame.height), style: UITableView.Style.plain)
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.register(ItemTableViewCell.self, forCellReuseIdentifier: "Cell")
        tableView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 180, right: 0)
        
        view.addSubview(tableView)
    }
    
    func createSpinnerView() {
        
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    
    func removeSpinnerView() {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = allTitle
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.999006331, green: 0.4324233532, blue: 0.08522579819, alpha: 1)
        self.navigationController?.navigationBar.tintColor = UIColor.white

        setupTableView()
        configureCategoryTextField()
        createSpinnerView()
        presenter?.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnTap = true
        view.alpha = 1.0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.alpha = 0.0
    }
}

// MARK: - UITableViewDataSource

extension MainViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ItemTableViewCell
        
        guard let itemWithCategoryName = presenter.itemWithCategoryNames?[indexPath.row] else { return cell }
        presenter.configure(cell: cell, itemWithCategoryName: itemWithCategoryName)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemsWithCategoryName.count
    }
}

// MARK: - UITableViewDelegate

extension MainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let itemWithCategoryName = itemsWithCategoryName[indexPath.row]
        let detailViewController = DetailWireframe.shared.createDetailModuleWith(itemWithCategoryName)
        detailViewController.detailPresenter.configure(view: detailViewController, itemWithCategoryName: itemWithCategoryName)
        self.view.endEditing(true)
        navigationController?.pushViewController(detailViewController, animated: true)
    }
}

extension MainViewController: MainViewDelegate {
    
    func show(itemsWithCategoryName: [ItemWithCategoryName]) {
        self.itemsWithCategoryName = itemsWithCategoryName
        tableView.reloadData()
        removeSpinnerView()
    }
    
    func failure(error: Error) {
        print(error.localizedDescription)
    }
}

extension MainViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        presenter.getResearchWith(searchText)
        if searchText == "" {
            navigationItem.title = allTitle
            self.view.endEditing(true)
        } else {
            navigationItem.title = searchText
        }
        tableView.reloadData()
    }
}

