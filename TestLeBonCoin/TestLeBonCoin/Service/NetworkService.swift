//
//  NetworkService.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

typealias DisplayItemsUseCaseCompletionHandler = (Result<[Item]?, Error>) -> Void
typealias DisplayCategoriesUseCaseCompletionHandler = (Result<[Category]?, Error>) -> Void

protocol NetworkServiceDelegate: class {
    func retrieveItems(completion: @escaping DisplayItemsUseCaseCompletionHandler)
    func retrieveCategories(completion: @escaping DisplayCategoriesUseCaseCompletionHandler)
}

class NetworkService: NetworkServiceDelegate {
    
    let apiClient: ApiClient
    
    init(apiClient: ApiClient) {
        self.apiClient = apiClient
    }
    
    func retrieveItems(completion: @escaping DisplayItemsUseCaseCompletionHandler) {
        
        let apiListRequest = ApiItemsRequest()
        apiClient.execute(request: apiListRequest) { (result: Result<ApiResponse<[Item]>, Error>) in
            switch result {
            case let .success(response):
                let items = response.entity
                completion(.success(items))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    func retrieveCategories(completion: @escaping DisplayCategoriesUseCaseCompletionHandler) {
        
        let apiListRequest = ApiCategoriesRequest()
        apiClient.execute(request: apiListRequest) { (result: Result<ApiResponse<[Category]>, Error>) in
            switch result {
            case let .success(response):
                let categories = response.entity
                completion(.success(categories))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
}
