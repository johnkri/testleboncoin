//
//  ApiRequest.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

protocol ApiRequestDelegate {
    var urlRequest: URLRequest { get }
}

struct ApiItemsRequest: ApiRequestDelegate {
    
    var urlRequest: URLRequest {
        
        let url = URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        return request
    }
    
}

struct ApiCategoriesRequest: ApiRequestDelegate {
    
    var urlRequest: URLRequest {
        
        let url = URL(string: "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        
        return request
    }
    
}
