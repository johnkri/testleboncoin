//
//  UIView+Utilities.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import UIKit

extension UIView {

    @discardableResult
    func pinToSuperview(edges: UIRectEdge = .all,
                        insets: UIEdgeInsets = .zero) -> [NSLayoutConstraint] {
        guard let superview = self.superview else { return [] }
        translatesAutoresizingMaskIntoConstraints = false
        let constraints: [NSLayoutConstraint] = [
            edges.contains(.top) ? pinTo(view: superview, attribute: .top, constant: insets.top) : nil,
            edges.contains(.left) ? pinTo(view: superview, attribute: .left, constant: insets.left) : nil,
            edges.contains(.bottom) ? pinTo(view: superview, attribute: .bottom, constant: -insets.bottom) : nil,
            edges.contains(.right) ? pinTo(view: superview, attribute: .right, constant: -insets.right) : nil,
            ].compactMap { $0 }
        return constraints
    }

    // MARK: - Private

    private func pinTo(view: UIView,
                       attribute: NSLayoutConstraint.Attribute,
                       constant: CGFloat) -> NSLayoutConstraint {
        let constraint = NSLayoutConstraint(
            item: self,
            attribute: attribute,
            relatedBy: .equal,
            toItem: view,
            attribute: attribute,
            multiplier: 1.0,
            constant: constant
        )
        constraint.isActive = true
        return constraint
    }
}
