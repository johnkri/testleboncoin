//
//  UIStackView+Utilities.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import UIKit

public extension UIStackView {

    convenience init(verticalViews: [UIView], spacing: CGFloat = 0) {
        self.init(arrangedSubviews: verticalViews)
        self.axis = .vertical
        self.spacing = spacing
    }

    convenience init(horizontalViews: [UIView], spacing: CGFloat = 0) {
        self.init(arrangedSubviews: horizontalViews)
        self.axis = .horizontal
        self.spacing = spacing
    }
}
