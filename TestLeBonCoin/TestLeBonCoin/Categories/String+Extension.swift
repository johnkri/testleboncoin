//
//  String+Extension.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

extension String {
    
    func convertDateFormat() -> String {
        let dateFormatter = DateFormatter()
        let tempLocale = dateFormatter.locale
        dateFormatter.locale = Locale(identifier: "fr_FR")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        let dateLast = dateFormatter.date(from: self)!
        dateFormatter.dateFormat = "dd-MM-yyyy HH:mm"
        dateFormatter.locale = tempLocale
        let dateString = dateFormatter.string(from: dateLast)
        return dateString
    }
}
