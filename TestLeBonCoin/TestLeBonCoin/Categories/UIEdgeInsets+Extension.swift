//
//  UIEdgeInsets+Utilities.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import UIKit

public extension UIEdgeInsets {

    init(value: CGFloat) {
        self.init(top: value, left: value, bottom: value, right: value)
    }
}



