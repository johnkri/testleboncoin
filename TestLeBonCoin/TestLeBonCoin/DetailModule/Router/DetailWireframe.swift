//
//  ModuleBuilder.swift
//  testleboncoin
//
//  Created by John Kricorian on 28/04/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import UIKit

protocol DetailWireframeDelegate: class {
    func createDetailModuleWith(_ itemWithCategoryName: ItemWithCategoryName?) -> DetailViewController
}

final class DetailWireframe: DetailWireframeDelegate {
    
    static let shared = DetailWireframe()
    
    init(){}
    
    func createDetailModuleWith(_ itemWithCategoryName: ItemWithCategoryName?) -> DetailViewController {
        let view = DetailViewController()
        var detailPresenter: DetailPresenterDelegate = DetailPresenterImplementation()
        
        view.detailPresenter = detailPresenter
        detailPresenter.detailView = view

        return view
    }
    
}

