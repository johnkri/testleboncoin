//
//  DetailPresenterImplementation.swift
//  TestLeBonCoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import Foundation

protocol DetailPresenterDelegate {
    func configure(view: DetailViewDelegate, itemWithCategoryName: ItemWithCategoryName)
    
    var detailView: DetailViewDelegate? { get set }
}


class DetailPresenterImplementation: DetailPresenterDelegate {
    
    var detailView: DetailViewDelegate?

    func configure(view: DetailViewDelegate, itemWithCategoryName: ItemWithCategoryName) {
        detailView?.configure(itemWithCategoryName: itemWithCategoryName)
    }
}
