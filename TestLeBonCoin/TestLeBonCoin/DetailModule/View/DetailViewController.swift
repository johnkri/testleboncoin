//
//  DetailViewController.swift
//  testleboncoin
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import UIKit

protocol DetailViewDelegate: class {
    func configure(itemWithCategoryName: ItemWithCategoryName)
}

class DetailViewController: UIViewController {
    
    var detailPresenter: DetailPresenterDelegate!
    
    let titleLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 24)
        lbl.textAlignment = .center
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        return lbl
        
    }()
    
    let line : UIView = {
        let l = UIView()
        l.sizeThatFits(CGSize(width: 260, height: 1))
        l.backgroundColor = .lightGray
        return l
        
    }()
        
    let iconImageView : UIImageView = {
        let imgView = UIImageView(image: #imageLiteral(resourceName: "Image"))
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        return imgView
    }()
        
    let descriptionLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 14)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        return lbl
    }()
    
    let priceLabel : UILabel = {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 12)
        lbl.textAlignment = .left
        lbl.numberOfLines = 0
        lbl.lineBreakMode = .byWordWrapping
        return lbl
    }()
    
    private func configureStackView() {
                
        let stackView = UIStackView()
        
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.alignment = .center
        stackView.spacing = 0
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(line)
        stackView.addArrangedSubview(iconImageView)
        stackView.addArrangedSubview(descriptionLabel)
        stackView.addArrangedSubview(priceLabel)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(stackView)
        
        stackView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        stackView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureStackView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnTap = true
        view.alpha = 1.0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.alpha = 0.0
    }
}

extension DetailViewController: DetailViewDelegate {
    
    func configure(itemWithCategoryName: ItemWithCategoryName) {
        titleLabel.text = itemWithCategoryName.title
        titleLabel.backgroundColor = UIColor.clear
        titleLabel.heightAnchor.constraint(equalToConstant: 60).isActive = true
        titleLabel.widthAnchor.constraint(equalToConstant: 280).isActive = true
        
        iconImageView.downloaded(from: itemWithCategoryName.image.thumb)
        iconImageView.backgroundColor = UIColor.clear
        iconImageView.heightAnchor.constraint(equalToConstant: 260).isActive = true
        iconImageView.widthAnchor.constraint(equalToConstant: 260).isActive = true
        
        line.heightAnchor.constraint(equalToConstant: 1).isActive = true
        line.widthAnchor.constraint(equalToConstant: 200).isActive = true
        
        descriptionLabel.text = itemWithCategoryName.description
        descriptionLabel.backgroundColor = UIColor.clear
        descriptionLabel.heightAnchor.constraint(equalToConstant: 100).isActive = true
        descriptionLabel.widthAnchor.constraint(equalToConstant: 280).isActive = true
        
        priceLabel.text = "Prix: \(itemWithCategoryName.price) euros"
        priceLabel.backgroundColor = UIColor.clear
        priceLabel.heightAnchor.constraint(equalToConstant: 100).isActive = true
        priceLabel.widthAnchor.constraint(equalToConstant: 280).isActive = true
        
    }
}
