//
//  TestLeBonCoinTests.swift
//  TestLeBonCoinTests
//
//  Created by John Kricorian on 01/05/2020.
//  Copyright © 2020 John Kricorian. All rights reserved.
//

import XCTest
@testable import TestLeBonCoin

class TestLeBonCoinTests: XCTestCase {

    let interactor = InteractorImplementation()

    func testCreateItemsArrayWithCategoryNameString() {
        
        // Given
        let image = Image(small: "small", thumb: "thumb")
        let items = [Item(imagesUrl: image, id: 1, title: "", price: 1, creationDate: "", isUrgent: true, categoryId: 1, description: "")]
        let categories = [Category(id: 1, name: "mode")]
        let categoryName = "mode"
        // When
        let itemWithCat = interactor.createItemsWithCategoryNameWith(items, and: categories)
        
        // Then
        XCTAssertEqual(itemWithCat.first?.categoryName, categoryName)
        
    }
    
    func testMostRecentDateAndUrgentSorting() {
        
        // Given
        let image = Image(small: "small", thumb: "thumb")
        let item1 = Item(imagesUrl: image, id: 1, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryId: 1, description: "")
        let item2 = Item(imagesUrl: image, id: 2, title: "", price: 1, creationDate: "2019-11-05T15:56:52+0000", isUrgent: false, categoryId: 1, description: "")
        let item3 = Item(imagesUrl: image, id: 3, title: "", price: 1, creationDate: "2013-11-05T15:56:35+0000", isUrgent: true, categoryId: 1, description: "")
        let item4 = Item(imagesUrl: image, id: 4, title: "", price: 1, creationDate: "2018-11-05T15:56:24+0000", isUrgent: false, categoryId: 1, description: "")
        
        let items = [item1,item2, item3, item4]
        
        // When
        let sortedItems = interactor.sorted(items)
        
        // Then
        let verifiedSortedItems = [item1, item3, item4, item2]
        XCTAssertEqual(sortedItems, verifiedSortedItems)
    }
    
    
    func testMostUrgenSorting() {
        
        // Given
        let image = Image(small: "small", thumb: "thumb")
        let item1 = Item(imagesUrl: image, id: 1, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryId: 1, description: "")
        let item2 = Item(imagesUrl: image, id: 2, title: "", price: 1, creationDate: "2019-11-05T15:56:52+0000", isUrgent: false, categoryId: 1, description: "")
        let item3 = Item(imagesUrl: image, id: 3, title: "", price: 1, creationDate: "2013-11-05T15:56:35+0000", isUrgent: true, categoryId: 1, description: "")
        let item4 = Item(imagesUrl: image, id: 4, title: "", price: 1, creationDate: "2018-11-05T15:56:24+0000", isUrgent: false, categoryId: 1, description: "")
        
        let items = [item1,item2, item3, item4]
        
        // When
        let sortedItems = interactor.sorted(items)
        
        // Then
        let isUrgent = true
        let isNotUrgent = false
        XCTAssertEqual(sortedItems.first?.isUrgent, isUrgent)
        XCTAssertEqual(sortedItems.last?.isUrgent, isNotUrgent)
    }
    
    func testFilterWithSearchText() {
        
        // Given
        let image = Image(small: "small", thumb: "thumb")
        let item1 = ItemWithCategoryName(image: image, id: 1, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Mode", description: "")
        let item2 = ItemWithCategoryName(image: image, id: 2, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Immobilier", description: "")
        let item3 = ItemWithCategoryName(image: image, id: 3, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Mode", description: "")
        let item4 = ItemWithCategoryName(image: image, id: 4, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Enfant", description: "")
        
        let items = [item1, item2, item3, item4]
        
        // When
        let itemsTofilter = interactor.filterItemWith("Mo", itemWithCategoryNames: items)
        
        // Then
        let fileredItems = [item1, item3]
        XCTAssertEqual(itemsTofilter, fileredItems)
    }
    
    func testReturnEmptyArrayWhenGivenAWrongSearchText() {
        
        // Given
        let image = Image(small: "small", thumb: "thumb")
        let item1 = ItemWithCategoryName(image: image, id: 1, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Mode", description: "")
        let item2 = ItemWithCategoryName(image: image, id: 2, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Immobilier", description: "")
        let item3 = ItemWithCategoryName(image: image, id: 3, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Mode", description: "")
        let item4 = ItemWithCategoryName(image: image, id: 4, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Enfant", description: "")
        
        let items = [item1, item2, item3, item4]
        
        // When
        let itemToFilter = interactor.filterItemWith("Modee", itemWithCategoryNames: items)
        
        // Then
        XCTAssertTrue(itemToFilter.isEmpty)

    }
    
    func testCheckAfterResearchIfFirstItemIsStillUrgent() {
        
        // Given
        let image = Image(small: "small", thumb: "thumb")
        let item1 = ItemWithCategoryName(image: image, id: 1, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Mode", description: "")
        let item2 = ItemWithCategoryName(image: image, id: 2, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: false, categoryName: "Enfant", description: "")
        let item3 = ItemWithCategoryName(image: image, id: 3, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: false, categoryName: "Enfant", description: "")
        let item4 = ItemWithCategoryName(image: image, id: 4, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Enfant", description: "")
        
        let items = [item1, item2, item3, item4]
        
        // When
        let itemsTofilter = interactor.filterItemWith("Enfa", itemWithCategoryNames: items)
        
        // Then
        XCTAssertTrue(itemsTofilter.last?.isUrgent == true)
    }
    
    func testCheckItemsArrayCountWhenResearchTextIsRemoved() {
        
        // Given
        let image = Image(small: "small", thumb: "thumb")
        let item1 = ItemWithCategoryName(image: image, id: 1, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Mode", description: "")
        let item2 = ItemWithCategoryName(image: image, id: 2, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: false, categoryName: "Enfant", description: "")
        let item3 = ItemWithCategoryName(image: image, id: 3, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: false, categoryName: "Enfant", description: "")
        let item4 = ItemWithCategoryName(image: image, id: 4, title: "", price: 1, creationDate: "2010-10-16T17:10:20+0000", isUrgent: true, categoryName: "Enfant", description: "")
        
        let items = [item1, item2, item3, item4]
        
        // When
        let itemsTofilter = interactor.filterItemWith("", itemWithCategoryNames: items)
        
        // Then
        XCTAssertEqual(items.count, itemsTofilter.count)
    }
}

